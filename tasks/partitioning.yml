# Check if step already completed and skip if so
- stat: path=/tmp/partitioning_completed
  register: partitioning_completed

# Wipe existing partition table and set GPT label
- name: Make Partition Labels
  command: "parted -s {{ main_disk }} mklabel gpt"
  when: partitioning_completed.stat.exists == False

# Create partitions
- name: Create GRUB BIOS partition
  when: partitioning_completed.stat.exists == False
  parted:
    device: "{{ main_disk }}"
    number: 1
    label: gpt
    part_end: 3MiB
    flags: [ bios_grub ]
    state: present

- name: Create boot partition
  when: partitioning_completed.stat.exists == False
  parted:
    device: "{{ main_disk }}"
    number: 2
    label: gpt
    part_start: 3MiB
    part_end: "{{ boot_part_end }}{{ boot_part_unit }}"
    state: present

- name: Create swap partition
  when: partitioning_completed.stat.exists == False
  parted:
    device: "{{ main_disk }}"
    number: 3
    label: gpt
    part_start: "{{ boot_part_end }}{{ boot_part_unit }}"
    part_end: "{{ swap_part_end }}{{ swap_part_unit }}"
    state: present

- name: Create root partition
  when: partitioning_completed.stat.exists == False
  parted:
    device: "{{ main_disk }}"
    number: 4
    label: gpt
    part_start: "{{ swap_part_end }}{{ swap_part_unit }}"
    state: present

# Format partitions
- name: Format boot Partition
  when: partitioning_completed.stat.exists == False
  filesystem:
    fstype: "{{ boot_fs }}"
    dev: "{{ main_disk }}2"
    force: "{{ boot_force_format }}"

- name: Format root Partition
  when: partitioning_completed.stat.exists == False
  filesystem:
    fstype: "{{ root_fs }}"
    dev: "{{ main_disk }}4"
    force: "{{ root_force_format }}"

# Create and enable swap
- name: Format swap
  command: "mkswap {{ main_disk }}3"
  when: partitioning_completed.stat.exists == False

- name: Enable swap
  command: "swapon {{ main_disk }}3"
  when: partitioning_completed.stat.exists == False

# Completed phase, create tmp file to prevent above steps from repeating
- name: Partitioning completed
  when: partitioning_completed.stat.exists == False
  copy:
    content: ""
    dest: /tmp/partitioning_completed
    force: no
    group: root
    owner: root
    mode: 0644
